'use strict';
const DeviceLog = require('./deviceLog.controller');
const prefix = "/api/deviceLog";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', DeviceLog.addDeviceLog);
    app.post(prefix+'/put', DeviceLog.update);
    app.post(prefix+'/get', DeviceLog.get);
    app.post(prefix+'/all', DeviceLog.getAll);
};