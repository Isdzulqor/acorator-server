'use strict';
const Common = require('../../config/common');
const Config = require('../../config/config');
const Jwt = require('jsonwebtoken');
const DeviceLog = require('./deviceLog.model').DeviceLog;
const privateKey = Config.key.privateKey;
const async = require('async');
const moment = require('moment-timezone');

exports.addDeviceLog = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    console.log("token : " + token);
    console.log("req.body : " + JSON.stringify(req.body));
    let createdData = req.body;
    createdData.created_by = decoded.tokenData.id;
    createdData.role = Config.role.deviceLog;

    DeviceLog.create(createdData, function (err, result) {
        if (!err) {
            result.password = Common.decrypt(result.password);
            response.is_success = true;
            response.description = "success";
            response.data = result;
            return res.json(response)
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response)
        }
    });
};

exports.get = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = new Object();

    if (req.body.deviceLog != null) {
        query._id = req.body.deviceLog;
        //common get data
        DeviceLog.get(query, function (err, result) {
            if (!err) {
                response.is_success = true;
                response.description = "success";
                response.data = result;
                return res.json(response);
            } else {
                response.is_success = false;
                esponse.description(err);
                return res.json(response);
            }
        });
    } else {
        //error
        response.is_success = false;
        response.description = "no bank account selected";
        response.data = null;
        return res.json(response);
    }
};

exports.update = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');

    let updatedData = req.body;
    updatedData.updated_by = decoded.tokenData.id;
    updatedData.updated_date = new Date() ;

    DeviceLog.updateById(req.params.id, updatedData, function (err, result) {
        if (!err) {
            response.is_success = true;
            response.description = "success";
            response.data = result;
            return res.json(response);
        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });

};

exports.getAll = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    DeviceLog.getAll(query, function (err, result) {
        if (!err) {
            response.is_success = true;
            response.description = "success";
            response.data = result;
            return res.json(response);
        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });
};

