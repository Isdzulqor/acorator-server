const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var DeviceLog = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    value: {
        type: String,
        required: true
    },

    device: {
        type: Schema.Types.ObjectId,
        ref: 'device',
        required: true
    },
});

DeviceLog.plugin(autoIncrement.plugin, {
    model: 'deviceLog',
    field: '_id'
});

DeviceLog.plugin(uniqueValidator);

DeviceLog.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    create: function(data, callback) {
        var deviceLog = new this(data);
        deviceLog.save(callback);
    }
};

var deviceLog = mongoose.model('deviceLog', DeviceLog);

/** export schema */
module.exports = {
    DeviceLog: deviceLog
};
