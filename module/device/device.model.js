const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var Device = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    code: {
        type: String,
        unique: true,
        required: true
    },

    name: {
        type: String,
        required: true
    },

    description: {
        type: String
    },

    state: {
        type: String,
        enum: [Config.module.device.state.connected,
            Config.module.device.state.disconnected],
        required: true,
        default: Config.module.device.state.disconnected
    },

    last_connection: {
        type: Date
    },

    dashboard: {
        type: Schema.Types.ObjectId,
        ref: 'dashboard',
        required: true
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },

    condition: {
        type: String,
        enum: [Config.module.device.condition.on,
            Config.module.device.condition.off],
        required: true,
        default: Config.module.device.condition.off
    },

    device_type: {
        type: Schema.Types.ObjectId,
        ref: 'deviceType',
        required: true
    },
});

Device.plugin(autoIncrement.plugin, {
    model: 'device',
    field: '_id'
});

Device.plugin(uniqueValidator);

Device.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    getAllByDashboard: function(query, callback) {
        this.find(query, callback);
    },
    getAllByUser: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var device = new this(data);
        device.save(callback);
    }
};

var device = mongoose.model('device', Device);

/** export schema */
module.exports = {
    Device: device
};
