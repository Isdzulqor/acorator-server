'use strict';
const Device = require('./device.controller');
const prefix = "/api/device";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', Device.addDevice);
    app.post(prefix+'/put', Device.update);
    app.post(prefix+'/get', Device.get);
    app.post(prefix+'/delete', Device.delete);
    app.post(prefix+'/all', Device.getAll);
};