'use strict';
const DeviceType = require('./deviceType.controller');
const prefix = "/api/deviceType";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', DeviceType.addDeviceType);
    app.post(prefix+'/put', DeviceType.update);
    app.post(prefix+'/get', DeviceType.get);
    app.post(prefix+'/delete', DeviceType.delete);
    app.post(prefix+'/all', DeviceType.getAll);
};