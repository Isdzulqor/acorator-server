'use strict';
const DetailIfttRule = require('./detailIfttRule.controller');
const prefix = "/api/detailIfttRule";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', DetailIfttRule.addDetailIfttRule);
    app.post(prefix+'/put', DetailIfttRule.update);
    app.post(prefix+'/get', DetailIfttRule.get);
    app.post(prefix+'/delete', DetailIfttRule.delete);
    app.post(prefix+'/all', DetailIfttRule.getAll);
};