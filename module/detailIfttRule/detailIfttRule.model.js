const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var DetailIfttRule = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id: false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    device: {
        type: Schema.Types.ObjectId,
        ref: 'device',
        required: true
    },

    state: {
        type: String,
        enum: [Config.module.detail_iftt_rule.state.if_this,
            Config.module.detail_iftt_rule.state.then_that],
        required: true
    },

    device_condition: {
        type: String,
        enum: [Config.module.detail_iftt_rule.device_condition.on,
            Config.module.detail_iftt_rule.device_condition.off],
        required: true,
        default: Config.module.detail_iftt_rule.device_condition.off
    },

    longtime:{
        type: Number
    },

    under_value:{
        type: Number
    },

    over_value:{
        type: Number
    },

    start_time:{
        type: Date
    },

    end_time:{
        type: Date
    },

    days:{
        type: Number
    },

    is_anytime:{
        type: Boolean,
        required: true,
        default: false
    }
});

DetailIfttRule.plugin(autoIncrement.plugin, {
    model: 'detailIfttRule',
    field: '_id'
});

DetailIfttRule.plugin(uniqueValidator);

DetailIfttRule.statics = {
    get: function (query, callback) {
        this.findOne(query, callback);
    },
    getAll: function (query, callback) {
        this.find(query, callback);
    },
    updateById: function (id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function (removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function (data, callback) {
        var detailDetailIfttRule = new this(data);
        detailDetailIfttRule.save(callback);
    }
};

var detailDetailIfttRule = mongoose.model('detailIfttRule', DetailIfttRule);

/** export schema */
module.exports = {
    DetailIfttRule: detailIfttRule
};
