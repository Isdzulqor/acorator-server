'use strict';
const DashboardActivity = require('./dashboardActivityActivity.controller');
const prefix = "/api/dashboardActivity";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', DashboardActivity.addDashboardActivity);
    app.post(prefix+'/put', DashboardActivity.update);
    app.post(prefix+'/get', DashboardActivity.get);
    app.post(prefix+'/delete', DashboardActivity.delete);
    app.post(prefix+'/all', DashboardActivity.getAll);
};