const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var DashboardActivity = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    name: {
        type: String,
        required: true
    },

    dashboard: {
        type: Schema.Types.ObjectId,
        ref: 'dashboard'
    },
});

DashboardActivity.plugin(autoIncrement.plugin, {
    model: 'dashboardActivity',
    field: '_id'
});

DashboardActivity.plugin(uniqueValidator);

DashboardActivity.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var dashboardActivity = new this(data);
        dashboardActivity.save(callback);
    }
};

var dashboardActivity = mongoose.model('dashboardActivity', DashboardActivity);

/** export schema */
module.exports = {
    DashboardActivity: dashboardActivity
};
