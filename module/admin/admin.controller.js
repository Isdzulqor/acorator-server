'use strict';
const Common = require('../../config/common');
const Config = require('../../config/config');
const Jwt = require('jsonwebtoken');
const Admin = require('./admin.model').Admin;
const privateKey = Config.key.privateKey;
const async = require('async');
const moment = require('moment-timezone');

exports.firstInstall = function (req, res) {
    let response = Config.base_response;
    let createdData = req.body;
    createdData.role = Config.role.super_admin;
    createdData.password = Common.encrypt(createdData.password);
    Admin.create(createdData, function (err, result) {
        if (!err) {

            let tokenData = {
                email: result.email,
                id: result._id,
                type: createdData.role,
                accesibilities: result.accesibilities,
            };

            let token = Jwt.sign({
                    exp: Config.key.tokenExpiry,
                    tokenData
                }
                , privateKey );

            Common.sentMailVerificationLink(result, token, (error, result) => {
                if (!error) {
                    console.log("sent Mail error : " + error);
                }
                else {
                    console.log("sent Mail done");
                }
            });
            response.is_success = true;
            response.description = "success";
            response.data = result;
            response.token = token;
            return res.json(response);
        } else {
            response.is_success = false;
            response.description = err;
            return res.json(response);
        }

    });

};

exports.addAdmin = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    console.log("token : " + token);
    console.log("req.body : " + JSON.stringify(req.body));
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            console.log("TIDAK ERROR : "+JSON.stringify(decoded));
            if (decoded.tokenData.type == Config.role.super_admin) {
                let createdData = req.body;
                createdData.created_by = decoded.tokenData.id;
                createdData.role = Config.role.admin;

                Admin.create(createdData, function (err, result) {
                    if (!err) {
                        result.password = Common.decrypt(result.password);
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                    } else {
                        response.is_success = false;
                        response.description = err;
                        response.data = null;
                    }
                    return res.json(response)
                });
            } else {
                response.is_success = false;
                response.description = Config.errors.unauthorized;
                response.data = null;
                return res.json(response)
            }
        }

        else {
            console.log("ERROR : "+JSON.stringify(decoded));
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response)
        }
    });
};

exports.get = function (req, res) {
    console.log("alhamdulillah");
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = new Object();

    if (req.body.admin != null) {
        console.log("alhamdulillah");
        query._id = req.body.admin;
        //common get data
        Jwt.verify(token, privateKey, (err, decoded) => {
            if (!err) {
                //common get data
                if (decoded.tokenData.type == Config.role.super_admin || (decoded.tokenData.type == Config.role.admin && decoded.tokenData.id == query._id)) {
                    Admin.get(query, function (err, result) {
                        if (!err) {
                            response.is_success = true;
                            response.description = "success";
                            response.data = result;
                            return res.json(response);
                        } else {
                            response.is_success = false;
                            esponse.description(err);
                            return res.json(response);
                        }
                    });
                } else {
                    response.is_success = false;
                    response.description(Config.errors.unauthorized);
                    return res.json(response);
                }
            } else {
                response.is_success = false;
                response.description = err;
                response.data = null;
                return res.json(response);
            }
        });
    } else {
        //sign in
        query.email = req.body.email;
        // query.password = req.body.password;
        console.log("json : " + JSON.stringify(query));
        Admin.get(query, function (err, result) {
            if (!err) {
                if (Common.decrypt(result.password) == req.body.password) {
                    let tokenData = {
                        email: result.email,
                        id: result._id,
                        type: result.role,
                        accesibilities: result.accesibilities,
                    };

                    let token = Jwt.sign({
                            exp: Config.key.tokenExpiry,
                            tokenData
                        }
                        , privateKey );

                    response.token = token;
                    response.is_success = true;
                    response.description = "success";
                    response.data = result;
                    return res.json(response);
                } else {
                    response.is_success = false;
                    response.description = "password is incorrect";
                    response.data = null;
                    return res.json(response);
                }
            } else {
                response.is_success = false;
                response.description(err);
                response.data = null;
                return res.json(response);
            }
        });
    }
};

exports.update = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            let updatedData = req.body;
            updatedData.updated_by = decoded.tokenData.id;
            updatedData.updated_date = new Date();

            if (decoded.tokenData.type == Config.role.super_admin || (decoded.tokenData.type == Config.role.admin && decoded.tokenData.id == query._id)) {
                Admin.updateById(req.params.id, updatedData, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }

        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });

};

/** removeAdmin function to get Admin by id. */
exports.delete = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.super_admin) {
                Admin.removeById({_id: req.body.admin}, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "";
                        response.data = result;
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                    }
                    return res.json(response);
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }


        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });
};

exports.getAll = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.super_admin) {
                Admin.getAll(query, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response);
        }
    });
};

exports.verifyEmail = (req, res) => {
    let response = Config.base_response;
    let query = new Object();

    Jwt.verify(req.params.token, privateKey, (err, decoded) => {
        if (!err) {
            query._id = decoded.tokenData.id;
            if (decoded.tokenData.type == Config.role.admin) {
                Admin.get(query, function (err, result) {
                    let updatedData = result;
                    updatedData.updated_by = decoded.tokenData.id;
                    updatedData.updated_date = new Date();
                    updatedData.isVerified = true;

                    if (!err) {
                        Admin.updateById(query._id, updatedData, function (err, result) {
                            if (!err) {
                                response.is_success = true;
                                response.description = "success";
                                response.data = null;
                                return res.json(response);
                            } else {
                                response.is_success = false;
                                response.description(err);
                                response.data = null;
                                return res.json(response);
                            }
                        });
                    } else {
                        response.is_success = false;
                        response.description(err);
                        return res.json(response);
                    }
                });
            }
        }

        else {
            response.is_success = false;
            response.description(err);
            return res.json(response);
        }
    })
};


exports.forgotPass = (req, res) => {
    let response = Config.base_response;
    let query = new Object();
    let adminTemp = new Admin;

    query = req.body.email;

    Admin.get(query, function (err, result) {
        if (!err) {
            response.is_success = true;
            response.description = "success";
            response.data = result;
            adminTemp = result;
            adminTemp.temp_password = Common.encrypt(Common.randomString(8));
            Admin.updateById(adminTemp._id, adminTemp, function (err, result) {
                if (!err) {
                    response.is_success = true;
                    response.description = "success";
                    response.data = null;

                    result.temp_password = Common.decrypt(adminTemp.temp_password);
                    Common.sentMailForgotPassword(result, (error, result) => {
                        if (!error) {
                            console.log("sent Mail error : " + error);
                        }
                        else {
                            console.log("sent Mail done");
                        }
                    });
                    return res.json(response);

                } else {
                    response.is_success = false;
                    response.description(err);
                    response.data = null;
                    return res.json(response);
                }
            });

            return res.json(response);
        } else {
            response.is_success = false;
            response.description(err);
            return res.json(response);
        }
    });

};
