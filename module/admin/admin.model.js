const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');

autoIncrement.initialize(db);

var Admin = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    email: {
        type: String,
        unique: true,
        required: true
    },

    fullname: {
        type: String,
        required: true
    },

    birth_date: Date,

    phone_number: {
        type: String,
        unique: true,
        sparse: true
    },

    password: {
        type: String,
        required: true,
        default: function () {
            return Common.encrypt(Common.randomString(8));
        }
    },

    temp_password: {
        type: String
    },

    isVerified: {
        type: Boolean,
        default: false
    },

    isActive: {
        type: Boolean,
        default: true
    },

    role: String
});

Admin.plugin(autoIncrement.plugin, {
    model: 'admin',
    field: '_id'
});
Admin.plugin(uniqueValidator);

Admin.statics = {
    firstInstall: function (requestData, callback) {
        this.create(requestData, callback);
    },
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },

    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var admin = new this(data);
        admin.save(callback);
    }
};

var admin = mongoose.model('admin', Admin);

/** export schema */
module.exports = {
    Admin: admin
};
