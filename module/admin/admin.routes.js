'use strict';
const Admin = require('./admin.controller');
const prefix = "/api/admin";
const Config = require("../../config/config");

module.exports = function(app){
    // API Server Endpoints
    app.post('/api/first-install', Admin.firstInstall);
    app.post(prefix+'/post', Admin.addAdmin);
    app.post(prefix+'/put', Admin.update);
    app.post(prefix+'/get', Admin.get);
    app.post(prefix+'/delete', Admin.delete);
    app.post(prefix+'/all', Admin.getAll);
    app.post(prefix+'/verifyEmail', Admin.verifyEmail);
    app.post(prefix+'/forgotPass', Admin.forgotPass);
    let response = Config.base_response;
    response.data = Config.module.transaction.status;
    app.post(prefix+'/tesTok', (req, res)=>{
        return res.json(response)
    });
};