'use strict';
const Dashboard = require('./dashboard.controller');
const prefix = "/api/dashboard";

module.exports = function(app){
    // API Server Endpoints
    // app.post(prefix+'/post', Dashboard.addDashboard);
    app.post(prefix+'/post', Dashboard.addDummyDashboard);
    app.post(prefix+'/put', Dashboard.update);
    app.post(prefix+'/get', Dashboard.get);
    app.post(prefix+'/delete', Dashboard.delete);
    // app.post(prefix+'/all', Dashboard.getAll);
    app.post(prefix+'/all', Dashboard.getAllDummy);
};