'use strict';
const Common = require('../../config/common');
const Config = require('../../config/config');
const Jwt = require('jsonwebtoken');
const Dashboard = require('./dashboard.model').Dashboard;
const privateKey = Config.key.privateKey;
const async = require('async');
const moment = require('moment-timezone');

exports.addDashboard = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    console.log("token : " + token);
    console.log("req.body : " + JSON.stringify(req.body));
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.user  &&
                Common.checkAccessibility(Config.module.accessibility.dashboard, decoded.tokenData.accesibilities)) {
                let createdData = req.body;
                createdData.created_by = decoded.tokenData.id;
                createdData.role = Config.role.dashboard;

                Dashboard.create(createdData, function (err, result) {
                    if (!err) {
                        result.password = Common.decrypt(result.password);
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response)
                    } else {
                        response.is_success = false;
                        response.description = err;
                        response.data = null;
                        return res.json(response)
                    }
                });
            } else {
                response.is_success = false;
                response.description = Config.errors.unauthorized;
                response.data = null;
                return res.json(response)
            }
        }

        else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response)
        }
    });
};

exports.get = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = new Object();

    if (req.body.dashboard != null) {
        query._id = req.body.dashboard;
        //common get data
        Jwt.verify(token, privateKey, (err, decoded) => {
            if (!err) {
                //common get data
                if (decoded.tokenData.type == Config.role.admin || (decoded.tokenData.type == Config.role.user &&
                        Common.checkAccessibility(Config.module.accessibility.dashboard, decoded.tokenData.accesibilities))) {
                    Dashboard.get(query, function (err, result) {
                        if (!err) {
                            response.is_success = true;
                            response.description = "success";
                            response.data = result;
                            return res.json(response);
                        } else {
                            response.is_success = false;
                            esponse.description(err);
                            return res.json(response);
                        }
                    });
                } else {
                    response.is_success = false;
                    response.description(Config.errors.unauthorized);
                    return res.json(response);
                }
            } else {
                response.is_success = false;
                response.description = err;
                response.data = null;
                return res.json(response);
            }
        });
    } else {
        //error
        response.is_success = false;
        response.description = "no bank account selected";
        response.data = null;
        return res.json(response);
    }
};

exports.update = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');

    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            let updatedData = req.body;
            updatedData.updated_by = decoded.tokenData.id;
            updatedData.updated_date = new Date() ;

            if (decoded.tokenData.type == Config.role.user &&
                Common.checkAccessibility(Config.module.accessibility.dashboard, decoded.tokenData.accesibilities)) {
                Dashboard.updateById(req.params.id, updatedData, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }

        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });

};

/** removeDashboard function to get Dashboard by id. */
exports.delete = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.user &&
                Common.checkAccessibility(Config.module.accessibility.dashboard, decoded.tokenData.accesibilities)) {
                Dashboard.removeById({_id: req.body.dashboard}, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }


        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });
};

exports.getAll = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.admin || (decoded.tokenData.type == Config.role.user &&
                Common.checkAccessibility(Config.module.accessibility.dashboard, decoded.tokenData.accesibilities))) {
                Dashboard.getAll(query, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response);
        }
    });
};

exports.getAllDummy = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    Dashboard.getAll(query, function (err, result) {
        if (!err) {
            response.is_success = true;
            response.description = "success";
            response.data = result;
            return res.json(response);
        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });
};

exports.addDummyDashboard = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    console.log("token : " + token);
    console.log("req.body : " + JSON.stringify(req.body));
    let createdData = req.body;
    createdData.created_by = "admin";
    Dashboard.create(createdData, function (err, result) {
        if (!err) {
            // result.password = Common.decrypt(result.password);
            response.is_success = true;
            response.description = "success";
            response.data = result;
            return res.json(response)
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response)
        }
    });
};