const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var Dashboard = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    name: {
        type: String,
        required: true
    },

    address: {
        type: String
    },

    latitude: {
        type: Number
    },

    longitude: {
        type: Number
    },

    ip_host: {
        type: String
    },
    
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }

    // user: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'user',
    //     // required: true
    // },
});

Dashboard.plugin(autoIncrement.plugin, {
    model: 'dashboard',
    field: '_id'
});

Dashboard.plugin(uniqueValidator);

Dashboard.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var dashboard = new this(data);
        dashboard.save(callback);
    }
};

var dashboard = mongoose.model('dashboard', Dashboard);

/** export schema */
module.exports = {
    Dashboard: dashboard
};
