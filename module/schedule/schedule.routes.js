'use strict';
const Schedule = require('./schedule.controller');
const prefix = "/api/schedule";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', Schedule.addSchedule);
    app.post(prefix+'/put', Schedule.update);
    app.post(prefix+'/get', Schedule.get);
    app.post(prefix+'/delete', Schedule.delete);
    app.post(prefix+'/all', Schedule.getAll);
};