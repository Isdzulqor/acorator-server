const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var Schedule = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    state: {
        type: String,
        enum: [Config.module.schedule.state.on,
            Config.module.schedule.state.off],
        required: true,
        default: Config.module.schedule.state.off
    },

    dashboard: {
        type: Schema.Types.ObjectId,
        ref: 'dashboard',
        required: true
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
});

Schedule.plugin(autoIncrement.plugin, {
    model: 'schedule',
    field: '_id'
});

Schedule.plugin(uniqueValidator);

Schedule.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var schedule = new this(data);
        schedule.save(callback);
    }
};

var schedule = mongoose.model('schedule', Schedule);

/** export schema */
module.exports = {
    Schedule: schedule
};
