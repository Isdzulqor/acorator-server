'use strict';
const Common = require('../../config/common');
const Config = require('../../config/config');
const Jwt = require('jsonwebtoken');
const User = require('./user.model').User;
const privateKey = Config.key.privateKey;
const async = require('async');
const moment = require('moment-timezone');

exports.addUser = function (req, res) {
    let response = Config.base_response;

    let createdData = req.body;
    createdData.role = Config.role.user;
    createdData.password = Common.encrypt(createdData.password);

    User.create(createdData, function (err, result) {
        if (!err) {
            let tokenData = {
                email: result.email,
                id: result._id,
                type: result.role,
                accesibilities: result.accesibilities,
                manager: result.manager
            };

            let token = Jwt.sign({
                    exp: Config.key.tokenExpiry,
                    tokenData
                }
                , privateKey );

            response.is_success = true;
            response.description = "success";
            response.data = result;
            response.token = token;

            return res.json(response)
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            response.token = null;
            return res.json(response)
        }
    });
};

exports.get = function (req, res) {
    console.log("alhamdulillah");
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = new Object();

    if (req.body.user != null) {
        console.log("alhamdulillah");
        query._id = req.body.user;
        //common get data
        Jwt.verify(token, privateKey, (err, decoded) => {
            if (!err) {
                //common get data
                if (decoded.tokenData.type == Config.role.user && decoded.tokenData.id == query._id) {
                    User.get(query, function (err, result) {
                        if (!err) {
                            response.is_success = true;
                            response.description = "success";
                            response.data = result;
                            return res.json(response);
                        } else {
                            response.is_success = false;
                            esponse.description(err);
                            return res.json(response);
                        }
                    });
                } else {
                    response.is_success = false;
                    response.description(Config.errors.unauthorized);
                    return res.json(response);
                }
            } else {
                response.is_success = false;
                response.description = err;
                response.data = null;
                return res.json(response);
            }
        });
    } else {
        //sign in
        query.email = req.body.email;
        // query.password = req.body.password;
        console.log("json : "+JSON.stringify(query));
        User.get(query, function (err, result) {
            if (!err) {
                console.log("tidak error : "+result);
                if(Common.decrypt(result.password) == req.body.password){
                    let tokenData = {
                        email: result.email,
                        id: result._id,
                        type: result.role,
                        accesibilities: result.accesibilities,
                        manager: result.manager
                    };

                    let token = Jwt.sign({
                            exp: Config.key.tokenExpiry,
                            tokenData
                        }
                        , privateKey );

                    response.token = token;
                    response.is_success = true;
                    response.description = "success";
                    response.data = result;
                } else {
                    response.is_success = false;
                    response.description = "password is incorrect";
                    response.data = null;
                }
                return res.json(response);
            } else {
                response.is_success = false;
                response.description(err);
                response.data = null;
                return res.json(response);
            }
        });
    }
};

exports.update = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            let updatedData = req.body;
            updatedData.updated_by = decoded.tokenData.id;
            updatedData.updated_date = new Date() ;

            if (decoded.tokenData.type == Config.role.user && decoded.tokenData.id == query._id) {
                User.updateById(req.params.id, updatedData, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                    }
                    return res.json(response);
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }

        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });

};

/** removeUser function to get User by id. */
exports.delete = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.super_admin) {
                User.removeById({_id: req.body.user}, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "";
                        response.data = result;
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                    }
                    return res.json(response);
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }


        } else {
            response.is_success = false;
            response.description(err);
            response.data = null;
            return res.json(response);
        }
    });
};

exports.getAll = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.super_admin) {
                User.getAll(query, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response);
        }
    });
};

exports.getAllByManager = function (req, res) {
    let response = Config.base_response;
    let token = req.get('Authorization');
    let query = req.body;

    Jwt.verify(token, privateKey, (err, decoded) => {
        if (!err) {
            if (decoded.tokenData.type == Config.role.user &&
                Common.checkAccessibility(Config.module.accessibility.user_management, decoded.tokenData.accesibilities)) {
                User.getAllByManager(query, function (err, result) {
                    if (!err) {
                        response.is_success = true;
                        response.description = "success";
                        response.data = result;
                        return res.json(response);
                    } else {
                        response.is_success = false;
                        response.description(err);
                        response.data = null;
                        return res.json(response);
                    }
                });
            } else {
                response.is_success = false;
                response.description(Config.errors.unauthorized);
                return res.json(response);
            }
        } else {
            response.is_success = false;
            response.description = err;
            response.data = null;
            return res.json(response);
        }
    });
};

exports.verifyEmail = (req, res) =>{
    let response = Config.base_response;
    let query = new Object();

    Jwt.verify(req.params.token, privateKey, (err, decoded) => {
        if (!err) {
            query._id = decoded.tokenData.id;
            if(decoded.tokenData.type == Config.role.user){
                User.get(query, function (err, result) {
                    let updatedData = result;
                    updatedData.updated_by = decoded.tokenData.id;
                    updatedData.updated_date = new Date() ;
                    updatedData.isVerified = true;

                    if (!err) {
                        User.updateById(query._id, updatedData, function (err, result) {
                            if (!err) {
                                response.is_success = true;
                                response.description = "success";
                                response.data = null;
                                return res.json(response);
                            } else {
                                response.is_success = false;
                                response.description(err);
                                response.data = null;
                                return res.json(response);
                            }
                        });
                    } else {
                        response.is_success = false;
                        response.description(err);
                        return res.json(response);
                    }
                });
            }
        }

        else {
            response.is_success = false;
            response.description(err);
            return res.json(response);
        }
    })
};


exports.forgotPass = (req, res) => {
    let response = Config.base_response;
    let query = new Object();
    let userTemp = new User;

    query = req.body.email;

    User.get(query, function (err, result) {
        if (!err) {
            response.is_success = true;
            response.description = "success";
            response.data = result;
            userTemp = result;
            userTemp.temp_password = Common.encrypt(Common.randomString(8));
            User.updateById(userTemp._id, userTemp, function (err, result) {
                if (!err) {
                    response.is_success = true;
                    response.description = "success";
                    response.data = null;
                    result.temp_password = Common.decrypt(userTemp.temp_password);
                    Common.sentMailForgotPassword(result, (error, result) => {
                        if (!error) {
                            console.log("sent Mail error : " + error);
                        }
                        else {
                            console.log("sent Mail done");
                        }
                    });
                    return res.json(response);

                } else {
                    response.is_success = false;
                    response.description(err);
                    response.data = null;
                    return res.json(response);
                }
            });

            return res.json(response);
        } else {
            response.is_success = false;
            response.description(err);
            return res.json(response);
        }
    });

};
