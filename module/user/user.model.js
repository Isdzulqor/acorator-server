const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');

autoIncrement.initialize(db);

var User = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    email: {
        type: String,
        unique: true,
        required: true
    },
    fullname: String,

    phone_number: {
        type: String,
        unique: true,
        sparse: true
    },

    password: {
        type: String,
        required: true
    },

    temp_password: {
        type: String
    },

    manager: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },

    role: String,

    accessibilities : [{ type : Schema.Types.ObjectId, ref: 'accesibility' }]

});

User.plugin(autoIncrement.plugin, {
    model: 'user',
    field: '_id'
});
User.plugin(uniqueValidator);

User.statics = {
    firstInstall: function (requestData, callback) {
        this.create(requestData, callback);
    },

    get: function(query, callback) {
        this.findOne(query, callback);
    },

    getAll: function(query, callback) {
        this.find(query, callback);
    },

    getAllByManager: function(query, callback) {
        this.find(query, callback);
    },

    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var user = new this(data);
        user.save(callback);
    }
};

var user = mongoose.model('user', User);

/** export schema */
module.exports = {
    User: user
};
