'use strict';
const User = require('./user.controller');
const prefix = "/api/user";

module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', User.addUser);
    app.post(prefix+'/put', User.update);
    app.post(prefix+'/get', User.get);
    app.post(prefix+'/delete', User.delete);
    app.post(prefix+'/all', User.getAll);
    app.post(prefix+'/allByManager', User.getAllByManager);
    app.post(prefix+'/verifyEmail', User.verifyEmail);
    app.post(prefix+'/forgotPass', User.forgotPass);
};