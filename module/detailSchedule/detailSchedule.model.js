const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var DetailSchedule = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    state: {
        type: String,
        enum: [Config.module.detail_schedule.state.on,
            Config.module.detail_schedule.state.off],
        required: true,
        default: Config.module.detail_schedule.state.off
    },

    start_date_time: {
        type: Date,
        default: new Date()
    },

    end_date_time: {
        type: Date,
        default: new Date()
    },

    schedule: {
        type: Schema.Types.ObjectId,
        ref: 'schedule',
        required: true
    },

    device: {
        type: Schema.Types.ObjectId,
        ref: 'device',
        required: true
    }

});

Schedule.plugin(autoIncrement.plugin, {
    model: 'detailSchedule',
    field: '_id'
});

Schedule.plugin(uniqueValidator);

Schedule.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },
    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var schedule = new this(data);
        schedule.save(callback);
    }
};

var Detailschedule = mongoose.model('detailSchedule', DetailSchedule);

/** export schema */
module.exports = {
    DetailSchedule: Detailschedule
};
