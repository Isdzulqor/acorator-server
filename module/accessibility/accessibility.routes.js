'use strict';
const Accessibility = require('./accessibility.controller');
const prefix = "/api/accessibility";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', Accessibility.addAccessibility);
    app.post(prefix+'/put', Accessibility.update);
    app.post(prefix+'/get', Accessibility.get);
    app.post(prefix+'/delete', Accessibility.delete);
    app.post(prefix+'/all', Accessibility.getAll);
};