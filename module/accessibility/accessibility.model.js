const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var Accessibility = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id:false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    code: {
        type: String,
        unique: true,
        required: true
    },

    description: {
        type: String,
        required: true
    }
});

Accessibility.plugin(autoIncrement.plugin, {
    model: 'accessibility',
    field: '_id'
});
Accessibility.plugin(uniqueValidator);

Accessibility.statics = {
    get: function(query, callback) {
        this.findOne(query, callback);
    },
    getAll: function(query, callback) {
        this.find(query, callback);
    },

    updateById: function(id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function(removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function(data, callback) {
        var accessibility = new this(data);
        accessibility.save(callback);
    }
};

var accessibility = mongoose.model('accessibility', Accessibility);

/** export schema */
module.exports = {
    Accessibility: accessibility
};
