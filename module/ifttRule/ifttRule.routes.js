'use strict';
const IfttRule = require('./ifttRule.controller');
const prefix = "/api/ifttRule";



module.exports = function(app){
    // API Server Endpoints
    app.post(prefix+'/post', IfttRule.addIfttRule);
    app.post(prefix+'/put', IfttRule.update);
    app.post(prefix+'/get', IfttRule.get);
    app.post(prefix+'/delete', IfttRule.delete);
    app.post(prefix+'/all', IfttRule.getAll);
};