const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    db = require('../../config/db').db;
uniqueValidator = require('mongoose-unique-validator');
const Common = require('../../config/common');
const moment = require('moment-timezone');
const Config = require('../../config/config');

autoIncrement.initialize(db);

var IfttRule = new Schema({
    created_date: {
        type: Date,
        default: new Date()
    },
    created_by: {
        _id: false,
        type: String
    },
    updated_date: {
        type: Date,
        default: new Date()
    },
    updated_by: {
        type: String,
        default: this._id
    },

    dashboard: {
        type: Schema.Types.ObjectId,
        ref: 'dashboard',
        required: true
    },

    iftt_setting: {
        type: Schema.Types.ObjectId,
        ref: 'ifttSetting'
    },

    state: {
        type: String,
        enum: [Config.module.iftt_rule.state.active,
            Config.module.iftt_rule.state.nonactive],
        required: true,
        default: Config.module.iftt_rule.state.nonactive
    }
});

IfttRule.plugin(autoIncrement.plugin, {
    model: 'ifttRule',
    field: '_id'
});

IfttRule.plugin(uniqueValidator);

IfttRule.statics = {
    get: function (query, callback) {
        this.findOne(query, callback);
    },
    getAll: function (query, callback) {
        this.find(query, callback);
    },
    updateById: function (id, updateData, callback) {
        this.update(id, {$set: updateData}, callback);
    },
    removeById: function (removeData, callback) {
        this.remove(removeData, callback);
    },
    create: function (data, callback) {
        var ifttRule = new this(data);
        ifttRule.save(callback);
    }
};

var ifttRule = mongoose.model('ifttRule', IfttRule);

/** export schema */
module.exports = {
    IfttRule: ifttRule
};
