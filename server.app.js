'use strict';

const express = require('express');
//cors setting
const cors = require('cors');
//cors setting
const config = require('./config/config');
const bodyParser = require('body-parser');
const app = express();
const db = require('./config/db');
const path = require('path');
const logger = require('morgan');
// const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');

// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'client/')));
app.use(logger('dev'));


//cors setting
const originsWhitelist = [
    'http://localhost:4200'      //this is my front-end url for development
];
const corsOptions = {
    origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
    },
    credentials:true
};
//cors setting

//here is the magic
app.use(cors(corsOptions));

app.get('*', function(req, res) {
    // res.sendFile(path.resolve('client/index.html')); // load the single view file (angular will handle the page changes on the front-end)
    res.json("Iskandar Dzulqornain"); // load the single view file (angular will handle the page changes on the front-end)
});

/** load routes*/
require('./module/user/user.routes')(app);
require('./module/dashboard/dashboard.routes')(app);

var port = config.server.port;


app.listen(process.env.PORT || port);

console.log('App started on port ' + port);
