var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var base_schema = {
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId
};

module.exports.base_schema = base_schema;
