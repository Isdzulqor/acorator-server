var mongoose = require("mongoose");
// var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var userSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    email:{
        type: String,
        required: true
    },
    name:String,
    phone:String,
    password:{
        type: String,
        required: true
    },
    active:{
        type: String,
        enum: ['ACTIVE', 'NONACTIVE']
    }

});

var user = mongoose.model("users", userSchema);
module.exports.user = user;