var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var detailIfttRuleSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    state:{
        type: String,
        enum: ['If This', 'Than That']
    },
    device_state:{
        type: String,
        enum: ['ON', 'OFF']
    },
    longtime: Number,       // jika ifThis
    under_value:Number,     //jika than that
    over_value:Number,	    //jika than that
    start_time:Date,	    //jika than that
    end_time:Date,	//jika than that
    days:Number,          //jika than that
    is_anytime:Boolean      //jika than that
});

var detailIfttRule = mongoose.model("detail_iftt_rules", detailIfttRuleSchema);
module.exports.detailIfttRule = detailIfttRule;