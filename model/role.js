var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var roleSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    code: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }

});

var role = mongoose.model("roles", roleSchema);
module.exports.role = role;