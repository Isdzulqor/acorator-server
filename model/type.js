var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var typeSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    name: {
        type: String,
        required: true
    },
    description: String

});

var type = mongoose.model("types", typeSchema);
module.exports.type = type;