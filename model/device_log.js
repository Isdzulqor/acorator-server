var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var deviceLogSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,


    value:Number,
    id_device:Schema.Types.ObjectId
});

var deviceLog = mongoose.model("device_logs", deviceLogSchema);
module.exports.deviceLog = deviceLog;