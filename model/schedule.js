var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var scheduleSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,


    state:{
        type:String,
        enum: ['ON', 'OFF']
    },
    id_dashboard:Schema.Types.ObjectId

});

var schedule = mongoose.model("schedules", scheduleSchema);
module.exports.schedule = schedule;