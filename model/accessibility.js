var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var accessibilitySchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    code: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }

});

var accessibility = mongoose.model("accessibilitys", accessibilitySchema);
module.exports.accessibility = accessibility;