var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var dashboardSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    name: String,
    address: String,
    latitude: Number,
    longitude: Number,
    id_user: Schema.Types.ObjectId,
    ip_host: String

});

var dashboard = mongoose.model("dashboards", dashboardSchema);
module.exports.dashboard = dashboard;