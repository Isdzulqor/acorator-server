var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var deviceSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    code: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    description: String,
    state: {
        type: String,
        enum: ['connected', 'disconnected']
    },
    last_connection:Date,
    id_dashboard:Schema.Types.ObjectId,
    condition:{
        type: String,
        enum: ['ON', 'OFF']
    },
    id_type:Schema.Types.ObjectId
});

var device = mongoose.model("devices", deviceSchema);
module.exports.device = device;