var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var ifttRuleSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    id_dashboard:Schema.Types.ObjectId,
    id_iftt_setting:Schema.Types.ObjectId,
    state:{
        type: String,
        enum: ['ACTIVE', 'NONACTIVE']
    }

});

var ifttRule = mongoose.model("iftt_rules", ifttRuleSchema);
module.exports.ifttRule = ifttRule;