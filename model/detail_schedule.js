var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var detailScheduleSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    state:{
        type: String,
        enum: ['ON', 'OFF']
    },
    start_date_time: Date,
    end_date_time: Date,
    id_device:Schema.Types.ObjectId
});

var detailSchedule = mongoose.model("detail_schedules", detailScheduleSchema);
module.exports.detailSchedule = detailSchedule;
