var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var dashboardActivitySchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,

    name: String
});

var dashboard_activity = mongoose.model("dashboard_activitys", dashboardActivitySchema);
module.exports.dashboardActivity = dashboardActivity;