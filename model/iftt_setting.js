var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var ifttSettingSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,


});

var ifttSetting = mongoose.model("iftt_settings", ifttSettingSchema);
module.exports.ifttSetting = ifttSetting;