var mongoose = require("mongoose");
var baseSchema = require("./base_schema").base_schema;
var Schema = mongoose.Schema;
var dashboardSettingSchema = new Schema({
    created_date: Date,
    updated_date: Date,
    created_by: Schema.Types.ObjectId,
    updated_by: Schema.Types.ObjectId,


});

var dashboardSetting = mongoose.model("dashboard_settings", dashboardSettingSchema);
module.exports.dashboardSetting = dashboardSetting;