module.exports = {
    server: {
            host: 'localhost',
            port: 3000
    },
    database: {
        host: 'localhost',
        port: 27017,
        db: 'acorator',
        url: 'mongodb://127.0.0.1:27017/acorator'
    },
    key: {
        privateKey: 'NiAnRoQlUzD_rAdNaKsI_dAmMaHuM',
        tokenExpiry: Math.floor(Date.now() / 1000) + (60 * 60) //1 hour
    },
    email: {
        username: "paperlessapproval@gmail.com",
        password: "bliblifutureprogram",
        verifyEmailUrl: "verify-email",
        resetEmailUrl: "reset",
        smtpHost: "smtp.gmail.com",
        smtpPort: 587,
        smtpSecure: false
    },
    role:{
        super_admin: "SUPER_ADMIN",
        admin: "ADMIN",
        user: "USER"
    },
    base_response :{
        is_success: false,
        description: "error",
        data: null,
        token: null
    },
    errors:{
        unauthorized: "You are not unauthorized"
    },
    module: {
        user: {
            status: {
                active: "ACTIVE",
                nonactive: "NON ACTIVE"
            }
        },
        device : {
            state: {
                connected: "CONNECTED",
                disconnected: "DISCONNECTED"
            },
            condition: {
                on: "ON",
                off: "OFF"
            }
        },
        schedule : {
            state: {
                on: "ON",
                off: "OFF"
            }
        },
        detail_schedule : {
            state: {
                on: "ON",
                off: "OFF"
            }
        },
        iftt_rule: {
            state: {
                active: "ACTIVE",
                nonactive: "NON ACTIVE"
            }
        },
        detail_iftt_rule: {
            state: {
                if_this: "IF THIS",
                then_that: "THEN THAT"
            },
            device_condition: {
                on: "ON",
                off: "OFF"
            }
        },
        accessibility: {
            dashboard: "DASHBOARD",
            schedule: "SCHEDULE",
            ifttt: "IFTTT",
            device: "DEVICE",
            user_management: "USER MANAGEMENT"
        }
    }
};
